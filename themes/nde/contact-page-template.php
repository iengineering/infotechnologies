<?php /* Template Name: Contact Page Template */

get_header(); ?>

    <main id="primary" class="site-main">
        <div class="nde-container">
            <div class="row contact-page-content mt-1">
                <div class="col-md-3 contact-img-container pr-1">
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/contact_placeholder.jpg' ?>">
                </div>
                <div class="col-md-9 contact-form-col d-flex align-items-center"><?php echo do_shortcode('[contact-form-7 id="249" title="Contact form 1"]') ?></div>
            </div>
        </div>
    </main><!-- #main -->

<?php
//get_sidebar();
get_footer();
