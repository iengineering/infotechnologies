<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package nde
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	

	<?php //nde_post_thumbnail();
    $id = get_the_ID();
    $category = get_the_category($id);
    $category_parent_id = $category[0]->category_parent;
    $getid = get_ancestors( $category_parent_id, 'category' );
    $highest_ancestor = $getid[count($getid) - 1];
    if($getid){
        $highest_ancestor = $getid[count($getid) - 1];
    }
    else{
        $highest_ancestor = $category_parent_id;
    }
    ?>

	<div class="entry-content">
		<?php
   	 $id =get_the_ID();
	
		if(($id == "2") || ($id == "252") || ($id == "186") ):?>
	
		<?php else: ?>
			<div class="col-header"><p class="p-2 mb-0"><?php echo get_cat_name( $highest_ancestor )?> - <?php the_title();?></p><button class="export-pdf">Export PDF</button></div>
			<?php endif; 
		the_content();

		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'nde' ),
				'after'  => '</div>',
			)
		);
		?>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'nde' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					wp_kses_post( get_the_title() )
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->