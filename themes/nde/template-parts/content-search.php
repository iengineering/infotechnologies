<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package nde
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
	$id = get_the_ID();
	$category = get_the_category($id); 
	$category_parent_id = $category[0]->category_parent;
	$getid = get_ancestors( $category_parent_id, 'category' ); 
	$highest_ancestor = $getid[count($getid) - 1];
	if($getid){
		$highest_ancestor = $getid[count($getid) - 1];
	}
	else{
		$highest_ancestor = $category_parent_id;
	}
	//echo get_cat_name( $highest_ancestor );

	?>
		<?php
        if (isset($_GET['cat-filter'])&& $_GET['cat-filter']=="all")
		{
            the_title( sprintf( '<h2 class="entry-title"><a href="'.get_permalink().'" rel="bookmark">'.get_cat_name( $highest_ancestor ).' - ', esc_url( get_permalink() ) ), '</a></h2>' );
        }
		else{
            if (!isset($_GET['cat-filter'])){
                the_title( sprintf( '<h2 class="entry-title"><a href="'.get_permalink().'" rel="bookmark">'.get_cat_name( $highest_ancestor ).' - ', esc_url( get_permalink() ) ), '</a></h2>' );
            }
               else{
                the_title( sprintf( '<h2 class="entry-title"><a href="'.get_permalink().'" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
            }
            
        }
         ?>

		<?php if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php
			nde_posted_on();
			nde_posted_by();
			?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	

	<?php //nde_post_thumbnail(); ?>

	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->

	<footer class="entry-footer">
		<?php nde_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->