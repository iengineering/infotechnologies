<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nde
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php wp_head(); ?>
    <script type="text/javascript">
        const stylesheet_directory_uri = "<?php echo get_stylesheet_directory_uri(); ?>";
    </script>
   <script async src="https://www.googletagmanager.com/gtag/js?id=UA-40700489-7"></script>

<script>

  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date()); 
  gtag('config', 'UA-40700489-7');

</script>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
    <div class="divNotification hide">
        <div class="divNotificationMessage RgbBeta">
            <b>Status:</b>
            <span id="msgNotificationBar">This feature/page is currently in Acceptance Release mode.</span>

                <span class="sReportBug"><a href="javascript:void(0);" onclick="fnIECQCQADevOpsReportIssue('cL2WxFhrVCk=');">Bug Report</a></span>
            <a href="javascript:void(0);" class="CloseNotification" title="Close">&nbsp;</a>
        </div>
        <div class="divNotificationMessageGap"></div>
    </div>
    <div id="fhwaheader">
        <div class="fhwacontainer">

            <div class="socialbtns">
                <ul class="dropdown dropdown-horizontal">
                    <li class="arrow mnabout"><a tabindex="3"
                                                 href="http://www.fhwa.dot.gov/about/"><span>About</span></a>
                        <ul>
                            <li><a title="FHWA Organization" href="http://www.fhwa.dot.gov/about/org/">FHWA
                                    Organization</a></li>
                            <li><a title="Headquarters Offices" href="http://www.fhwa.dot.gov/about/hq/">Headquarters
                                    Offices</a></li>
                            <li><a title="Field Offices" href="http://www.fhwa.dot.gov/about/field.cfm">Field
                                    Offices</a></li>
                            <li><a title="Careers" href="http://www.fhwa.dot.gov/careers/">Careers</a></li>
                            <li><a title="Strategic Plan" href="http://www.fhwa.dot.gov/policy/fhplan.htm">Strategic
                                    Plan</a></li>
                            <li><a title="Business Opportunities" href="http://www.fhwa.dot.gov/about/business.cfm">Business
                                    Opportunities</a></li>
                            <li><a title="Staff Directories" href="http://www.fhwa.dot.gov/about/staff.cfm">Staff
                                    Directories</a></li>
                            <li><a title="Highway History" href="http://www.fhwa.dot.gov/infrastructure/history.cfm">Highway
                                    History</a></li>
                        </ul>
                    </li>
                    <li class="arrow mnprograms"><a tabindex="4"
                                                    href="http://www.fhwa.dot.gov/programs/"><span>Programs</span></a>
                        <ul>
                            <li><a title="Acquisition Management" href="http://www.fhwa.dot.gov/aaa/">Acquisition
                                    Management</a></li>
                            <li><a title="Civil Rights" href="http://www.fhwa.dot.gov/civilrights/">Civil Rights</a>
                            </li>
                            <li><a title="Federal Lands Highway" href="http://flh.fhwa.dot.gov/">Federal Lands
                                    Highway</a></li>
                            <li><a title="Infrastructure"
                                   href="http://www.fhwa.dot.gov/infrastructure/">Infrastructure</a></li>
                            <li><a title="Innovative Program Delivery" href="http://www.fhwa.dot.gov/ipd/">Innovative
                                    Program Delivery</a></li>
                            <li><a title="Operations" href="http://ops.fhwa.dot.gov/">Operations</a></li>
                            <li><a title="Planning, Environment, and Realty" href="http://www.fhwa.dot.gov/hep/">Planning,
                                    Environment, and Realty</a></li>
                            <li><a title="Policy" href="http://www.fhwa.dot.gov/policy/">Policy</a></li>
                            <li><a title="Research, Development &amp; Technology"
                                   href="http://www.fhwa.dot.gov/research/">Research, Development &amp; Technology</a>
                            </li>
                            <li><a title="Safety" href="http://safety.fhwa.dot.gov/">Safety</a></li>
                            <li><a title="Browse by Topics" href="http://www.fhwa.dot.gov/resources/topics/">Browse by
                                    Topics</a></li>
                        </ul>
                    </li>
                    <li class="arrow mnresources"><a tabindex="5" href="http://www.fhwa.dot.gov/resources/"><span>Resources</span></a>
                        <ul>
                            <li><a title="Core Highway Topics" href="http://www.fhwa.dot.gov/resources/topics/">Core
                                    Highway Topics</a></li>
                            <li><a title="Federal-Aid Essentials" href="http://www.fhwa.dot.gov/federal-aidessentials/">Federal-Aid
                                    Essentials</a></li>
                            <li><a title="Publications &amp; Statistics"
                                   href="http://www.fhwa.dot.gov/resources/pubstats/">Publications &amp; Statistics</a>
                            </li>
                            <li><a title="Laws &amp; Regulations" href="http://www.fhwa.dot.gov/resources/legsregs/">Laws
                                    &amp; Regulations</a></li>
                            <li><a title="Policy &amp; Guidance Center" href="http://www.fhwa.dot.gov/pgc/">Policy &amp;
                                    Guidance Center</a></li>
                            <li><a title="Professional Development"
                                   href="http://www.fhwa.dot.gov/resources/training.cfm">Professional Development</a>
                            </li>
                            <li><a title="Resource Center" href="http://www.fhwa.dot.gov/resourcecenter/">Resource
                                    Center</a></li>
                            <li><a title="Accessibility Resource Library" href="http://www.fhwa.dot.gov/accessibility/">Accessibility
                                    Resource Library</a></li>
                            <li><a title="FHWA Research Library" href="http://www.fhwa.dot.gov/research/library/">FHWA
                                    Research Library</a></li>
                        </ul>
                    </li>
                    <li class="arrow mnbriefing"><a tabindex="6" href="http://www.fhwa.dot.gov/briefingroom/"><span>Briefing Room</span></a>
                        <ul>
                            <li><a title="Press Releases" href="http://www.fhwa.dot.gov/briefingroom/releases/">Press
                                    Releases</a></li>
                            <li><a title="Speeches &amp; Testimony"
                                   href="http://www.fhwa.dot.gov/briefingroom/speeches/">Speeches &amp; Testimony</a>
                            </li>
                            <li><a title="Photos" href="http://www.flickr.com/photos/fhwa/" rel="noopener noreferrer"
                                   target="_blank">Photos</a></li>
                            <li><a title="Videos"
                                   href="http://www.youtube.com/playlist?list=PL5_sm9g9d4T2eif-WaS6jb1Xtt7T5ce62"
                                   rel="noopener noreferrer" target="_blank">Videos</a></li>
                            <li><a title="Media Contacts" href="http://www.fhwa.dot.gov/briefingroom/contacts.cfm">Media
                                    Contacts</a></li>
                                   
                        </ul>
                    </li>
                    <li class="mncontact"><a tabindex="7" href="http://www.fhwa.dot.gov/research/tfhrc/contact/"><span>Contact</span></a>
                    </li>
                    <li class="mncontact"><a tabindex="8" href="https://search.usa.gov/search?utf8=%E2%9C%93&affiliate=dot-fhwa"><span>Search FHWA</span></a>
                    </li>
                    <li class="social-icons"><a title="Like Us on Facebook" tabindex="9"
                                                href="http://www.facebook.com/pages/Federal-Highway-Administration/175380479155058"
                                                rel="noopener noreferrer" target="_blank"><img alt="Facebook"
                                                                                               src="<?php echo get_template_directory_uri() . '/assets/images/facebook_med.png' ?>"></a><a
                                title="Follow Us on Twitter" tabindex="10" href="https://twitter.com/USDOTFHWA"
                                rel="noopener noreferrer" target="_blank"><img alt="Twitter"
                                                                               src="<?php echo get_template_directory_uri() . '/assets/images/twitter_med.png' ?>"></a><a
                                title="Watch Us on YouTube" tabindex="11" href="http://www.youtube.com/user/USDOTFHWA"
                                rel="noopener noreferrer" target="_blank"><img alt="YouTube"
                                                                               src="<?php echo get_template_directory_uri() . '/assets/images/youtube_med.png' ?>"></a>
                                                                               <a
                                title="See Our Photos on Flickr" tabindex="12" href="http://www.flickr.com/photos/fhwa/"
                                rel="noopener noreferrer" target="_blank"><img alt="Flickr"
                                                                               src="<?php echo get_template_directory_uri() . '/assets/images/flickr_med.png' ?>"></a>
                                                                               <a
                                title="Connect with Us on LinkedIn" tabindex="12" href="https://www.linkedin.com/company/federal-highway-administration/"
                                rel="noopener noreferrer" target="_blank"><img alt="Flickr"
                                                                               src="<?php echo get_template_directory_uri() . '/assets/images/linkedin_med.png' ?>"></a>
                    </li>
                </ul>
            </div>
            <a tabindex="2" href="http://www.fhwa.dot.gov/">
                <img class="imgdotfhwasm" alt="U.S. Department of Transportation/Federal Highway Administration"
                     src="<?php echo get_template_directory_uri() . '/assets/images/imgdotfhwasm.png' ?>"></a>
        </div>
    </div>
    <header id="masthead" class="site-header">
        <div class="nde-container py-3">
            <div class="row m-0 justify-content-between">
                <div class="site-branding col-4 p-0">
                    <a href="<?php echo home_url() ?>">
                        <img src="<?php echo get_template_directory_uri() . '/assets/images/fhwa_infotechnology_logo.png' ?>"
                             width="280">
                    </a>
                </div><!-- .site-branding -->
                <div class="col-8 p-0 text-right responsive_menu">


                    <button class="navbar-toggler float-right pr-0 hamburger-menu collapsed" type="button" data-toggle="collapse"
                            data-target="#responsive-menu" aria-controls="bs-example-navbar-collapse-1"
                            aria-expanded="false" aria-label="Toggle navigation">
                     <img class="hamburger-icon" src="<?php echo get_template_directory_uri() . '/assets/images/hamburger.png' ?>"  width="20">
                        <img class="close-icon" src="<?php echo get_template_directory_uri() . '/assets/images/icon-close.png' ?>"
                             width="20">
                    </button>


                    <button class="navbar-toggler float-right px-1 responsive-search" type="button"
                            data-toggle="collapse" data-target="#responsive-searchbar"
                            aria-controls="bs-example-navbar-collapse-2" aria-expanded="false"
                            aria-label="Toggle navigation">
                        <img src="<?php echo get_template_directory_uri() . '/assets/images/searchicon.png' ?>"
                             width="20">
                    </button>
                        <?php wp_nav_menu(array(
                              'theme_location' => 'menu-2',
                            'container_class' => 'top-menu',
                            'menu_class' => 'top-right-menu'
                        )) ?>
                    <div class="search-form">

                        <form class="d-inline-flex" action="<?php echo home_url() ?>" method="get">
                            <input type="text" class="border-grey px-1 font-12" name="s" id="search" placeholder="Search"
                                   value="<?php the_search_query(); ?>" required/>
                            <input type="submit" class="img-btn" alt="Search"
                                  value="Go"/>
                        </form>
                    </div>
                </div>

            </div>

        </div>

        <div id="responsive-menu" class="menu-section collapse navbar-collapse show">
            <div class="nde-container">
                <nav id="site-navigation" class="main-navigation">

                    <?php
                    wp_nav_menu(
                        array(
                            'theme_location' => 'menu-1',
                            'menu_id' => 'primary-menu',
                            'container' => 'div'
                        )
                    );
                    ?>
                </nav><!-- #site-navigation -->
            </div>
        </div>


        <div id="responsive-searchbar" class="menu-section collapse navbar-collapse">
            <div class="nde-container text-center">
                <form class="d-inline-flex searchForm" action="<?php echo home_url() ?>" method="get">
                <input type="text" class="border-grey px-1 font-12" name="s" id="search" placeholder="Search"
                                   value="<?php the_search_query(); ?>" required/>
                            <input type="submit" class="img-btn" alt="Search"
                                  value="Go"/>
                </form>
            </div>
        </div>
    </header><!-- #masthead -->