<?php
/**
 * nde functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package nde
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.2' );
}
// Add category support for pages
if (! function_exists('category_support_for_pages')):
    function category_support_for_pages(){
        register_taxonomy_for_object_type('category', 'page');
    }
endif;
add_action('init', 'category_support_for_pages');

if ( ! function_exists( 'nde_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function nde_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on nde, use a find and replace
		 * to change 'nde' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'nde', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'nde' ),
                'menu-2' => esc_html__( 'Top Right', 'nde' ),
                'menu-footer' => esc_html__( 'Footer', 'nde' ),
                'menu-footer-2' => esc_html__( 'Footer Two', 'nde' )
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'nde_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);

	}
endif;
add_action( 'after_setup_theme', 'nde_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function nde_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'nde_content_width', 640 );
}
add_action( 'after_setup_theme', 'nde_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function nde_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'nde' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'nde' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

    register_sidebar(
        array(
            'name'          => esc_html__( 'Footer Widget Area', 'nde' ),
            'id'            => 'footer-widget',
            'description'   => esc_html__( 'Add widgets to footer.', 'nde' ),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        )
    );
}
add_action( 'widgets_init', 'nde_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function nde_scripts() {

	// Icons and font cdn
	wp_enqueue_style('nde-font-samily', 'https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700;900&display=swap', array());
	
    wp_enqueue_script( 'nde-bugreport-js', get_template_directory_uri() . '/assets/js/IECGetSupport.js', true );
      wp_enqueue_script( 'nde-main-js', get_template_directory_uri() . '/assets/js/main.js', array('jquery'), _S_VERSION, true );
       wp_enqueue_script( 'calender-js','https://cdnjs.cloudflare.com/ajax/libs/datepicker/1.0.10/datepicker.min.js', array('jquery'), true );
    wp_localize_script('nde-main-js', 'filter_pages', array('ajax_url' => admin_url('admin-ajax.php')));

	// Bootstrap
    wp_enqueue_style('bootstrap-style', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap.min.css');
    wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/assets/bootstrap/js/bootstrap.bundle.js');
     wp_enqueue_script('clipboard-js','https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.10/clipboard.min.js');
	wp_enqueue_style( 'dashicons' );

	//wp_enqueue_script( 'nde-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), _S_VERSION, true );

    wp_enqueue_style( 'nde-style', get_stylesheet_uri(), array(), _S_VERSION );
    wp_style_add_data( 'nde-style', 'rtl', 'replace' );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'nde_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}
add_action('init','disable_kses_if_allowed');

function disable_kses_if_allowed() {
	if (current_user_can('unfiltered_html')) {
		// Disables Kses only for textarea saves
		foreach (array('pre_term_description', 'pre_link_description', 'pre_link_notes', 'pre_user_description') as $filter) {
			remove_filter($filter, 'wp_filter_kses');
		}
	}

	// Disables Kses only for textarea admin displays
	foreach (array('term_description', 'link_description', 'link_notes', 'user_description') as $filter) {
		remove_filter($filter, 'wp_kses_data');
	}
}


  function theme_modify_search( $query ) {
 
	if ($query->is_search && !is_admin() ) {
        $query->set('post_type','page');		
		
        $exclude_ids = array( 757, 67 , 250,770,252,2,69,3,759,761,71,186 ); // Array of the ID's to exclude
		$query->set( 'post__not_in', $exclude_ids );
		if (isset($_GET['cat-filter'])){
			$cat_id = $_GET['cat-filter'];
			$query->set('cat' , $cat_id);
		}
    }
 
return $query;
 
}
add_action( 'pre_get_posts', 'theme_modify_search' );
function filter_media_comment_status( $open, $post_id ) {
    $post = get_post( $post_id );
    if( $post->post_type == 'attachment' ) {
        return false;
    }
    return $open;
}
add_filter( 'comments_open', 'filter_media_comment_status', 10 , 2 );

add_filter( 'document_title_parts', 'filter_document_title_parts' );
function filter_document_title_parts( $title_parts ) {
	$id = get_the_ID();
    $category = get_the_category($id);
	if($category[0]){
		$category_parent_id = $category[0]->category_parent;
		$getid = get_ancestors( $category_parent_id, 'category' );
		$highest_ancestor = $getid[count($getid) - 1];
		if($getid){
			$highest_ancestor = $getid[count($getid) - 1];
		}
		else{
			$highest_ancestor = $category_parent_id;
		}
		$page_title = get_cat_name( $highest_ancestor );
		$title_parts['title'] = $page_title; 
	}
    return $title_parts; 
}


// Hook the appropriate WordPress action
//add_action('init', 'prevent_wp_login');

function prevent_wp_login() {
    // WP tracks the current page - global the variable to access it
    global $pagenow;
    // Check if a $_GET['action'] is set, and if so, load it into $action variable
    $action = (isset($_GET['action'])) ? $_GET['action'] : '';
    // Check if we're on the login page, and ensure the action is not 'logout'
    if( $pagenow == 'wp-login.php') {
        // Load the home page url
		$sign_in_url = site_url().'/signin';
        //$page = get_bloginfo('');
        // Redirect to the home page
        wp_redirect($sign_in_url);
        // Stop execution to prevent the page loading for any reason
        exit();
    }
}

add_filter( 'login_url', 'my_login_page', 10, 2 );
function my_login_page( $login_url, $redirect ) {
  return home_url( '/signin/?redirect_to=' . $redirect );
}

function infotechnology_login_fn() {
ob_start();
 if ( is_user_logged_in() ) {
	$admin_url = home_url().'/wp-admin/';
	wp_redirect($sign_in_url);
	exit();
	//print_r("<script>location.href = '".$admin_url."'</script>");
} else {
  $args = array(
    'echo'            => true,
    'redirect'        => get_permalink( get_the_ID()),
    'remember'        => true,
    'value_remember'  => true,
  );
 
   wp_login_form( $args );
}
return ob_get_clean();
}
add_shortcode( 'infotechnology_login', 'infotechnology_login_fn' );

add_action( 'wp_logout','wpdocs_ahir_redirect_after_logout' );
function wpdocs_ahir_redirect_after_logout() {
    wp_safe_redirect( home_url() );
    exit();
}
add_action( 'wp_login_failed', 'my_front_end_login_fail' );  // hook failed login

function my_front_end_login_fail( $username ) {
   $referrer = site_url().'/signin/';  // where did the post submission come from?
   // if there's a valid referrer, and it's not the default log-in screen
   if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
      wp_redirect( $referrer . '?login=failed' );  // let's append some information (login=failed) to the URL for the theme to use
      exit;
   }
}

add_action( 'wp_authenticate', 'catch_empty_user', 1, 2 );

function catch_empty_user( $username, $pwd ) {
 if ( empty( $username ) ||  empty( $pwd )) {
     $referrer = site_url().'/signin/';  // where did the post submission come from?
   // if there's a valid referrer, and it's not the default log-in screen
   if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
      wp_redirect( $referrer . '?login=empty' );  // let's append some information (login=failed) to the URL for the theme to use
      exit;
   }
 }
}

function custom_print_errors() {
     if(isset($_GET['login']) && $_GET['login'] == 'failed')
    {
        ?>
            <div class="aa_error">
                <strong>ERROR:</strong> Incorrect Email Address or Password, Please try again!
            </div>
        <?php
    }else if(isset($_GET['login']) && $_GET['login'] == 'empty'){
		   ?>
            <div class="aa_error">
                <strong>ERROR:</strong> The Email Address or Password field is empty.
            </div>
        <?php
	}
}



/* 
function login_recaptcha_script() { 
  wp_register_script("recaptcha_login", "https://www.google.com/recaptcha/api.js"); 
  wp_enqueue_script("recaptcha_login"); 
} 
add_action("login_enqueue_scripts", "login_recaptcha_script"); 
function display_login_captcha() { ?> 
  <div class="g-recaptcha" data-sitekey="<?php echo get_option('captcha_site_key'); ?>"></div> 
<?php } 
add_action( "login_form", "display_login_captcha" ); 
function verify_login_captcha($user, $password) { 
  if (isset($_POST['g-recaptcha-response'])) { 
  $recaptcha_secret = get_option('captcha_secret_key'); 
  $response = wp_remote_get("https://www.google.com/recaptcha/api/siteverify?secret=". $recaptcha_secret ."&response=". $_POST['g-recaptcha-response']); 
  $response = json_decode($response["body"], true); 
  if (true == $response["success"]) { 
  return $user; 
  } else { 
  return new WP_Error("Captcha Invalid", __("<strong>ERROR</strong>: You are a bot")); 
  }  
  } else { 
  return new WP_Error("Captcha Invalid", __("<strong>ERROR</strong>: You are a bot. If not then enable JavaScript")); 
  }   
} 
add_filter("wp_authenticate_user", "verify_login_captcha", 10, 2);  */