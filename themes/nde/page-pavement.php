<?php get_header();
$taxonomy_name = 'category';
function nde_get_select_options($id=0, $taxonomy_name){
   
    $parent_id =   get_terms(
        $taxonomy_name,
        array(
            'parent' => $id,
            'hide_empty' => 0
        )
    );   
     
    foreach ($parent_id as $child_id){
            echo '<li id="' . $child_id->term_id . '"><input type="radio" id="test1" name="material-group"> <label for="test1"></label>' . $child_id->name . '</li>';
    }
}
function nde_get_select_options_dropdown($id=0, $taxonomy_name){
   
    $parent_id =   get_terms(
        $taxonomy_name,
        array(
            'parent' => $id,
            'hide_empty' => 0
        )
    );   
    echo '<option>All</option>';
     
    foreach ($parent_id as $child_id){
            echo '<option value="' . $child_id->term_id . '">' . $child_id->name . '</option>';
    }
}
    $all_pages =get_posts( array(
        'post_type' => 'page',
        'numberposts' => -1,
        'orderby' => 'title',
        'order'   => 'asc',
        'tax_query' => array(
            array(
            'taxonomy' => 'category',
            'field' => 'term_id',
            'terms' => 53
            )
        )
            ));
//echo sizeof($all_pages);
?>

<div class="nde-container">
    <div class="row m-0 flex-wrap-inherit">
        <div class="col-md-3 px-0 filter-side-bar">
            <div class="col-header filter-form-header d-flex align-items-center justify-content-between p-2">
                <p class="m-0 p-0">Find Technology</p>                
                <img class="reset-form px-1" termid="53" src="<?php echo get_template_directory_uri().'/assets/images/clearfilter.gif'; ?>" title="Reset Filter">
            </div>
            <div class="filter-form-mobile form-container-mobile">

                <label class="p-2">Asset Type</label>
                <select id="asset-options" class="select-filter-list" >
                    <option value="<?php echo home_url().'/bridge'?>">Bridge</option>
                    <option value="<?php echo home_url().'/pavement'?>" selected>Pavements</option>
                    <option value="<?php echo home_url().'/tunnel'?>">Tunnel</option>
                </select>

                <!--Label and fields for material-->
                <label for="material" class="p-2">Material</label>
                <select id="material-option" class="select-filter-list" >
                <?php nde_get_select_options_dropdown(53, $taxonomy_name); ?>
                </select>

                <!--Label and fields for Structure Element-->
                <label for="structure-element" class="p-2">Structural Element</label>
                <select id="structure-element-option" class="select-filter-list disableFilter">
              			<option>All</option>
                </select>

                <!--Label and fields for Types of investigation-->
                <label for="types-of-investigation" class="p-2">Target of Investigation</label>
                <select  id="types-of-investigation-option" class="select-filter-list disableFilter">
               			<option>All</option>
                </select>
                
<div class="related-technology">
                    <label for="recommendede-element" class="p-2">Applicable Technologies</label>
                    <select id="recommended_technologies_mobile" class="select-filter-list get-single-page">
                        <option>All</option>
                    </select>
                </div>

            </div>
            <div class="filter-form form-container">

                <label class="p-2">Asset Type</label>
              
                   <ul class="select-filter-list asset-type">
                    <li id="28"><a href="<?php echo home_url().'/bridge'?>"><input type="radio" id="test1" value="<?php echo home_url().'/bridge'?>" name="technology-assettype" ><span  class="black-color">Bridge</span></a></li>
                    <li id="53"><a href="<?php echo home_url().'/pavement'?>"><input type="radio" id="test1" value="<?php echo home_url().'/pavement'?>" name="technology-assettype" checked><span class="black-color">Pavements</span></a></li>
                    <li id="122"><a href="<?php echo home_url().'/tunnel'?>"><input type="radio" id="test1" value="<?php echo home_url().'/tunnel'?>" name="technology-assettype"><span class="black-color">Tunnel</span></a></li>
                </ul>
              
                
                    <!--Label and fields for material-->
                    <label for="material" class="p-2">Material</label>
                    <ul id="material" class="select-filter-list" >
                        <?php nde_get_select_options(53, $taxonomy_name); ?>
                    </ul>
                   
                    <!--Label and fields for Structure Element-->
                    <label for="structure-element" class="p-2">Structural Element <img src="<?php echo get_template_directory_uri().'/assets/images/info.png'?>" class="info-img" class="tooltip-color" data-toggle="tooltip" data-placement="right" title="Select Material to list available options for Structural Element." width="18"></label>
                    <ul id="structure-element" class="select-filter-list mb-5p">
                      
                    </ul>

                    <!--Label and fields for Types of investigation-->
                    <label for="types-of-investigation" class="p-2">Target of Investigation <img src="<?php echo get_template_directory_uri().'/assets/images/info.png'?>" class="info-img" class="tooltip-color" data-toggle="tooltip" data-placement="right" title="Select Material and Structural Element to list available options for Target of Investigation." width="18"></label>
                    <ul  id="types-of-investigation" class="select-filter-list mb-5p">
                        
                    </ul>
                    <div class="related-technology">
                        <label for="recommendede-element" class="p-2">Applicable Technologies <img src="<?php echo get_template_directory_uri().'/assets/images/info.png'?>" class="info-img" class="tooltip-color" data-toggle="tooltip" data-placement="right" title="Select Material, Structural Element and Target of Investigation to list available options for Applicable Technologies." width="18"></label>
                        <ul class="card-list">
                        </ul>
                    </div>

              
            </div>
        </div>

        <div class="pr-0 padding-left-5 w-100">
            <div class="col-header">
                <p class="m-0 p-2">Pavements Non-Destructive Evaluation Technologies</p>
            </div>
            <div class="filtered-content" id="single-page-content">
             
                   <?php  foreach($all_pages as $key =>$page):                   
                        ?>
                       
                        <div class="card get-single-page" data-postid="<?php echo $page->ID ?>">
                      <div class="box-icon">
                          <?php $image_url =  get_template_directory_uri().'/assets/images/pavement-icon.png' ?>
                          <img src="<?php echo $image_url?>" alt="" class="card-img-top float-left" width="60">
                          <p><?php echo $page->post_title ?></p>
                        <img src="<?php echo get_template_directory_uri().'/assets/images/arrow-right.png'?>" class="float-right" width="24" height="16">
                        </div>
                         
                           
                        </div>
                       
                    <?php endforeach;?>
           
            </div>
        </div>
    </div>
</div>

<?php get_footer() ?>