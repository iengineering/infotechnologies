
<?php
/*
* Template Name: Login Page
*/
?>

<?php 
if ( is_user_logged_in() ) {
	$sign_in_url = site_url().'/wp-admin';
        //$page = get_bloginfo('');
        // Redirect to the home page
        wp_redirect($sign_in_url);
        // Stop execution to prevent the page loading for any reason
        exit();
} else {
get_header();

?>
<div class="wp_login_form">
<div class="form_heading"> Login Form </div>
	<div class="wp_login_form_row">
	<?php
	$args = array(
		'echo'            => true,
		'redirect'        => get_permalink( get_the_ID()),
		'remember'        => true,
		'label_username' => __( 'Email Address:' ),
        'label_password' => __( 'Password:' ),
		'value_remember'  => false,
	  );
	   wp_login_form( $args );
	   custom_print_errors();
	?>
	</div>
</div>

<?php 
}

get_footer() ?>