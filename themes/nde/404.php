<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package nde
 */

get_header();
?>

<main id="primary" class="site-main searchpage">
		<div class="nde-container">

		<section class="error-404 not-found">
			<header class="page-header">
				<h4 class="page-title"><?php esc_html_e( 'Sorry! That page can&rsquo;t be found.', 'nde' ); ?></h4>
			</header><!-- .page-header -->

			<div class="page-content">			
					

			</div><!-- .page-content -->
		</section><!-- .error-404 -->

	</main><!-- #main -->

<?php
get_footer();
