<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nde
 */

?>

<footer id="colophon" class="site-footer">
    <div class="nde-container">
        <div class="row">
            <div class="footer-logo-col mt-2 mr-3 pl-3 mb-sm-2">
                <?php //if (is_active_sidebar('footer-widget')):
                   // dynamic_sidebar('footer-widget');
               // endif; ?>
               <img src="<?php echo get_template_directory_uri().'/assets/images/FHWA_Logo_footer.png'?>" width="150">
            </div>
            <div class="col-md-7 footer-menu-col">
                <?php wp_nav_menu(
                    array(
                        'theme_location' => 'menu-footer',
                        'menu_id'        => 'footer-menu',
                    )
                ); ?>
                <div class="footer-info footer-address">
                    <?php wp_nav_menu(
                        array(
                            'theme_location' => 'menu-footer-2',
                            'menu_id'        => 'footer-menu-2',
                        )
                    ); ?>
                </div>
            </div>
        </div><!-- .site-info -->
    </div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
