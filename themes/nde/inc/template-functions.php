<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package nde
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function nde_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'nde_body_classes' );


/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function nde_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'nde_pingback_header' );
add_action('wp_ajax_nopriv_single_page_pdf_generate', 'single_page_pdf_generate');
add_action('wp_ajax_single_page_pdf_generate', 'single_page_pdf_generate');
function single_page_pdf_generate()
{
    $post_content = !empty($_POST['id']) ? wp_unslash($_POST['id']) : null;
    $page_name = !empty($_POST['pagename']) ? $_POST['pagename'] : null;
    require_once dirname(__FILE__) . '/mpdf-development/vendor/autoload.php';
    if(empty($page_name)){
        $page_name = "asd.pdf";
    }
    else{
        $page_name = $page_name.".pdf";
    }
    $rootpath  = $_SERVER['DOCUMENT_ROOT'];
    $domain_path = $_SERVER['SERVER_NAME'];
    $post_content = str_replace($rootpath,$domain_path,$post_content);
    $post_content = str_replace(array("\r", "\n"), '', $post_content);
    $pdf_dir = dirname(__FILE__) . "/mpdf-development/Generatedpdfs/".$page_name;
    $public_path =get_template_directory_uri() . "/inc/mpdf-development/Generatedpdfs/".$page_name;
    $mpdf = new \Mpdf\Mpdf();
    $mpdf->curlAllowUnsafeSslRequests = true;
    $mpdf->WriteHTML($post_content);
    $pdf_link =  $mpdf->Output($pdf_dir,'F');
    echo json_encode( $public_path);
    die();
}
// Get single page with Ajax
add_action('wp_ajax_nopriv_nde_get_single_page', 'nde_get_single_page');
add_action('wp_ajax_nde_get_single_page', 'nde_get_single_page');
function nde_get_single_page()
{
    $post_id = !empty($_POST['post_id']) ? $_POST['post_id'] : null;
    $term_id = !empty($_POST['term_id']) ? $_POST['term_id'] : null;
    $parentterm_id = !empty($_POST['parent_id']) ? $_POST['parent_id'] : null;
    $material_id = !empty($_POST['material_id']) ? $_POST
	['material_id'] : null;
    $structure_element = !empty($_POST['structure_element']) ? $_POST
	['structure_element'] : null;
    $type_investigation = !empty($_POST['type_investigation']) ? $_POST['type_investigation'] : null;
    $breadcrumb = "";
    
    if($parentterm_id){
        $parent_term = get_term( $parentterm_id, 'category' );        
        $breadcrumb .='<li class="breadcrumb-item top-term-link"><a termid='.$parentterm_id.' href="javascript:void(0)">'.$parent_term->name.'</a></li>';
    }
    if($material_id){
        $term_material = get_term( $material_id, 'category' );			
        $breadcrumb .='<li class="breadcrumb-item materialterm"><a id='.$material_id.' href="javascript:void(0)">'.$term_material->name.'</a></li>';
    }    
    
	if($structure_element){
		$hide_structure= "";
		$term_structure_element = get_term( $structure_element, 'category' );
		$desc_structure_element = $term_structure_element->description;
		$name_structure_element = $term_structure_element->name;
        $breadcrumb .='<li class="breadcrumb-item structureterm"><a id='.$structure_element.'   href="javascript:void(0)">'.$name_structure_element.'</a></li>';

	}
	else{
		$hide_structure= "hide";
		$desc_structure_element = "";
		$name_structure_element= "";

	}
    
	if($type_investigation){
		$hide_investigation = "";
		$term_type_investigation = get_term( $type_investigation, 'category' );
		$desc_type_investigation = $term_type_investigation->description;
		$name_type_investigation = $term_type_investigation->name;
        $breadcrumb .='<li class="breadcrumb-item investigationterm"><a id='.$type_investigation.'  href="javascript:void(0)">'.$name_type_investigation.'</a></li>';
	}
	else{
		$hide_investigation= "hide";
		$desc_type_investigation ="";
		$name_type_investigation="";

	}

	
 
    $data = array();
    $cat_name = array();
    $page = get_post($post_id);
     $modified_date = get_the_modified_date($d, $post_id );
    $breadcrumb .='<li class="breadcrumb-item"><a class="text-decoration-none"  href="javascript:void(0)">'.$page->post_title.'</a></li>';
    $data[] = '
        <div class="single-page-content desktop-view">
        
        <div class="breadcrumb-container">
       
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb secondary-color">
            <li class="breadcrumb-item"><a>NDE</a></li>
            '.$breadcrumb.'
            </ol>
        </nav>
        </div>
            <div class="col-header mb-0 px-0 justify-content-between">
               
                <ul class="nav nav-pills" id="single-page-tabs" role="tablist">
                    <li class="nav-item">
                        <a href="#title-content" class="tab-links nav-link active rounded-0" id="title-tab" data-toggle="pill" role="tab" aria-controls="title-content" aria-selected="true"> Technology</a>
                    </li>
                    <li class="nav-item '.$hide_investigation.'">
                        <a href="#tgt-of-inv" class="tab-links nav-link rounded-0" id="tgt-of-inv-tab" data-toggle="pill" role="tab" aria-controls="tgt-of-inv" aria-selected="true">Target of Investigation</a>
                    </li>
                    <li class="nav-item '.$hide_structure.'">
                        <a href="#struct-elem" class="tab-links nav-link rounded-0" id="struct-elem-tab" data-toggle="pill" role="tab" aria-controls="struct-elem" aria-selected="true">Structural Element</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="title-content" role="tabpanel"><div class="col-header"><p class="p-2">'.$page->post_title.'</p><!-- <button class="test technology-link" data-placement="bottom" data-clipboard-text="'.get_post_permalink( $post_id).'">Share</button> --><button class="export-pdf">Export PDF</button></div>' . $page->post_content . '</div>
                <div class="tab-pane fade" id="tgt-of-inv" role="tabpanel"><div class="col-header"><p class="p-2">'.$name_type_investigation.'</p><button class="export-pdf">Export PDF</button></div>'.$desc_type_investigation.'</div>
                <div class="tab-pane fade" id="struct-elem" role="tabpanel"><div class="col-header"><p class="p-2">'.$name_structure_element.'</p><button class="export-pdf">Export PDF</button></div>' . $desc_structure_element . '</div>
            </div> <p class="mb-0 mt-1 ml-1">Last updated on '.$modified_date.'</p>             
        </div>
        <div class="single-page-content mobile-view">
        <div class="breadcrumb-container">        
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb secondary-color">
            <li class="breadcrumb-item"><a>NDE</a></li>
            '.$breadcrumb.'
            </ol>
        </nav>
        </div>
        <div id="accordion">
        <div class="card ">
          <div class="card-header" data-toggle="collapse" href="#collapseOne">
          
              Technology
           
          </div>
          <div id="collapseOne" class="collapse show" data-parent="#accordion">
            <div class="card-body">
            <div class="col-header"><p class="p-2">'.$page->post_title.'</p><button class="export-pdf">Export PDF</button></div>' . $page->post_content . '
            </div>
          </div>
        </div>
        <div class="card '.$hide_investigation.'">
          <div class="card-header collapsed"  data-toggle="collapse" href="#collapseTwo">
          
          Target of Investigation
         
          </div>
          <div id="collapseTwo" class="collapse" data-parent="#accordion">
            <div class="card-body">
            <div class="col-header"><p class="p-2">'.$name_type_investigation.'</p><button class="export-pdf">Export PDF</button></div>'.$desc_type_investigation.'
            </div>
          </div>
        </div>
        <div class="card '.$hide_structure.'">
          <div class="card-header collapsed" data-toggle="collapse" href="#collapseThree">
          
          Structural Element
          </div>
          <div id="collapseThree" class="collapse" data-parent="#accordion">
            <div class="card-body">
            <div class="col-header"><p class="p-2">'.$name_structure_element.'</p><button class="export-pdf">Export PDF</button></div>' . $desc_structure_element . '
            </div>
          </div>
        </div>
      </div>
        </div>

            ';
    $all_pages = get_posts(array(
        'post_type' => 'page',
        'numberposts' => -1,
        'orderby' => 'title',
        'order'   => 'asc',
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field' => 'term_id',
                'terms' => $term_id
            )
        )
    ));
    $pages = array();
    
    if (!empty ($all_pages)) :
        foreach ($all_pages as $key => $detail):
            $image = get_template_directory_uri() . '/assets/images/bridge-icon.png';
            if($detail->ID == $post_id){
				$activeclass = "active";
                $buttonchecked= "checked";
                $selecteddropdown = "selected";
			}
			else{
				$activeclass = "";
                $buttonchecked= "";
                $selecteddropdown = "";
			}
			$pages[] = '<li class="get-single-page recomended-list '.$activeclass.'" data-postid="' . $detail->ID . '"><input type="radio" id="test1" name="recommended-group" '.$buttonchecked.'> <label for="test1"></label>'. $detail->post_title . '</li>';
            $recommended_technology_options[] = '<option value="' . $detail->ID . '" '.$selecteddropdown.'>' . $detail->post_title . '</option>';
        endforeach;
    endif;
    echo json_encode(array("page_result" => $data,"recommended_technologies_mobile"=> $recommended_technology_options ,"all_pages" => $pages, "terms" => $cat_name));
    //echo json_encode($term_ids);
    die();
}

add_action('wp_ajax_nopriv_get_selected_categories', 'get_selected_categories');

// For the users that are  logged in:  
add_action('wp_ajax_get_selected_categories', 'get_selected_categories');
function get_selected_categories()
{
    //die();
    $get_id = $_POST['id'];
    $cat_name = $_POST['cat_name'];
    if (empty($get_id)) {
        $get_id = 0;
    }
    $parent_id = get_terms(
        'category',
        array(
            'parent' => $get_id,
            'hide_empty' => 0
        )
    );
    
    $all_pages = get_posts(array(
        'post_type' => 'page',
        'numberposts' => -1,
        'orderby' => 'title',
        'order'   => 'asc',
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field' => 'term_id',
                'terms' => $get_id
            )
        )
    ));
    $pages = array();
    $recommended_technology_options = array();
    $recommended_technology_options[] = '<option disabled selected>All</option>';
    if (!empty ($all_pages)) :
        foreach ($all_pages as $key => $page):

            $category = get_the_category($page->ID); 
            $category_parent_id = $category[0]->category_parent;
            //echo $category_parent_id;
            $getid = get_ancestors( $category_parent_id, 'category' ); 
            
            if($getid){
                $highest_ancestor = $getid[count($getid) - 1];
                $get_cat_name = get_cat_name( $highest_ancestor );
            }
            else{
                $highest_ancestor = $category_parent_id;
                $get_cat_name = get_cat_name( $highest_ancestor );
            }
            if($get_cat_name=="Bridge"){
                $image_url = get_template_directory_uri().'/assets/images/bridge-icon.png';
            }
            if($get_cat_name=="Pavements"){
                $image_url = get_template_directory_uri().'/assets/images/pavement-icon.png';
            }
            if($get_cat_name=="Tunnel"){
                $image_url = get_template_directory_uri().'/assets/images/tunnel-icon.png';
            }
            
            $pages[] ='<div class="card get-single-page" data-postid='.$page->ID.'>
            <div class="box-icon">
                 
                <img src='.$image_url.' alt="" class="card-img-top float-left" width="60">
                <p>'.$page->post_title.'</p>
              <img src="'.get_template_directory_uri().'/assets/images/arrow-right.png" class="float-right" width="24" height="16">
              </div>
                  
               
                 
              </div>';
              $recommended_list[] = '<li class="get-single-page recomended-list" data-postid="' . $page->ID . '"><input type="radio" id="test1" name="recommended-group" '.$buttonchecked.'> <label for="test1"></label>'. $page->post_title . '</li>';
              $recommended_technology_options[] = '<option value="' . $page->ID . '">' . $page->post_title . '</option>';
        endforeach;

    else:
        $pages[] = "<h4>Sorry, no data was found.</h4>";
    endif;
    $option_arr = array();
    $mobile_dropdown_option = array('<option>All</option>');
    $mobile_dropdown_option_targetOfarr =  array('<option>All</option>');
    if($cat_name=="Material"){
        $group_name="group-structure";
    }
    else{
        $group_name="group-target";
    }
    $catNameArr = [];
    $targetOfArr = [];
    foreach ($parent_id as $child_id) {

        $option_arr[] = '<li id="' . $child_id->term_id . '"><input type="radio" id="test1" name="'.$group_name.'"> <label for="test1"></label>' . $child_id->name . '</li>';
        $mobile_dropdown_option[] = '<option value="' . $child_id->term_id . '">' . $child_id->name . '</option>';      
        
        
    }
    echo json_encode(array("cat_result" => $option_arr, "all_pages" => $pages,"terms_options" => $mobile_dropdown_option,"targetOfterms"=>$targetOfArr,"targetoftermsMobile"=>$mobile_dropdown_option_targetOfarr,"mobile_recommended_list"=>$recommended_technology_options,"desktop_recommended_tile"=>$recommended_list));
    die();
}