﻿/*********************************************/
/********************DevOps*******************/
/*********************************************/

function fnIECQCQADevOpsReportIssue(ProjectID) {
	var targetUrl = "https://qcqa.iengineering.com/QCQA/IssuesReport.aspx?";
	var sReferrerURL = document.URL;
    sReferrerURL = encodeURIComponent(sReferrerURL);

    targetUrl += "ProjectID="+ ProjectID;
    targetUrl += "&Referrer=" + sReferrerURL;

    var leftpos = (screen.width / 2) - (680 / 2);
    var toppos = (screen.height / 2) - (700 / 2)
    var windowprops = "height=702,width=725,scrollbars=no,menubars=no,resizable=yes,scrollbars=yes,left=" + leftpos + ",top=" + toppos
    popupWindow = window.open(targetUrl, "ReportIssue", windowprops);
    return false;
}

/*********************************************/
/**************QC/QA Center ISSUE*************/
/*********************************************/

function fnIECSupportIssue(SiteID, SystemID, ModuleID, IsDropDown, UserName, UserEmail, UserPhone) {
    var targetUrl = "http://qcqa.iengineering.com/QCQA/BugReport.aspx?Action=Add";
    var sReferrerURL = document.URL;
    sReferrerURL = encodeURIComponent(sReferrerURL);


    if (ValidateInput(SiteID, 1, 'Incorrect value for Site ID.') == true) {
        targetUrl += "&SiteID=" + SiteID;
    }
    else {
        return false;
    }

    if (ValidateInput(SystemID, 1, 'Incorrect value for System ID.') == true) {
        targetUrl += "&SystemID=" + SystemID;
    }
    else {
        return false;
    }

    if (ModuleID != null && ModuleID != "" && ModuleID != "0") {
        if (ValidateInput(IsDropDown, 1, 'Incorrect value for Module Dropdown Option.') == true) {
            if (IsDropDown == "0" || IsDropDown == "1") {
                if (ValidateInput(ModuleID, 1, "Incorrect value for Module ID.") == true) {
                    if (IsDropDown == "0")
                        targetUrl += "&ModuleID=" + ModuleID;
                    else
                        targetUrl += "&ModuleIDD=" + ModuleID;
                }
                else {
                    return false;
                }
            }
            else {
                alert("Incorrect value for Module Dropdown Option.");
                return false;
            }
        }
        else {
            return false;
        }
    }

    if (UserName.length > 0 && UserEmail.length > 0) {
        targetUrl += "&Name=" + UserName + "&Email=" + UserEmail;
        //return False;
    }

    if (UserPhone.length > 0) {
        targetUrl += "&Phone=" + UserPhone;
    }
    targetUrl += "&Referrer=" + sReferrerURL;

    var leftpos = (screen.width / 2) - (680 / 2);
    var toppos = (screen.height / 2) - (700 / 2)
    var windowprops = "height=702,width=725,scrollbars=no,menubars=no,resizable=yes,scrollbars=yes,left=" + leftpos + ",top=" + toppos
    popupWindow = window.open(targetUrl, "SupportIssue", windowprops);
    return false;
}

/*********************************************/
/**************Validation*********************/
/*********************************************/

function ValidateInput(curObj, nMinLength, paramMessage) {
    // Boolean - true if field is validated, otherwise false
    var bisVarified = true;

    if (curObj == null) {
        bisVarified = false;
    }

    if (bisVarified == true) {
        TrimString(curObj);
    }

    var re;
    re = /^[1-9]*[0-9]+$/;
    if (bisVarified == true) {
        if (curObj.match(re) == null)
            bisVarified = false;
    }

    // If field length is less than minimum length, the generate error, and set focus back to field
    // NOTE: If field is nullable and empty, then this code is never reached!
    if ((bisVarified == true) && (curObj.length < nMinLength)) {
        bisVarified = false;
    }

    // If field is not validate, then generate an error, and return false
    if (bisVarified == false) {
        alert(paramMessage);
        return false;
    }

    // True, if everything has been successful
    return bisVarified;
}

// Trims spaces, vertical/horizontal tabs, newline and carriage returns
// This function fixes the actual value in the text box
function TrimString(curObj) {
    if (curObj != null) {
        var re;
        re = /^[ \f\n\r\t\v]+/;
        curObj = curObj.replace(re, "");
        re = /[ \f\n\r\t\v]+$/;
        curObj = curObj.replace(re, "");
    }
}