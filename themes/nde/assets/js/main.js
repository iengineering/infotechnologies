(function ($) {
    jQuery(document).ready(function () {
    $('.wpcf7-submit').on('click', function (e) {
     var currentDate = new Date(); 
               
   var day = ("0" + currentDate.getDate()).slice(-2);
   var month = ("0" + (currentDate.getMonth() + 1)).slice(-2);
   var today = (day)+"/"+(month)+"/"+currentDate.getFullYear();   
   var hours = currentDate.getHours();
  var minutes = currentDate.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  $('#datess').val(today+" "+strTime);
});
              

            function setTooltip(btn, message) {
              btn.tooltip('hide')
                .attr('data-original-title', message)
                .tooltip('show');
            }

            function hideTooltip(btn) {
              setTimeout(function() {
                btn.tooltip('hide');
              }, 1000);
            }

            // Clipboard

            var clipboard = new Clipboard('.technology-link');

            clipboard.on('success', function(e) {
                var btn = $(e.trigger);
              setTooltip(btn, 'URL Copied');
              hideTooltip(btn);
            });
        $(document).on("mouseover", '.technology-link', function () {
           // alert();
  			 $(".technology-link").tooltip("hide");
		});
        jQuery('.CloseNotification').click(function (){
                    jQuery('.divNotification').hide();
                 });
        
         $('input[type="radio"][name="material-group"]').prop("checked", false);
        if ($(window).width() < 769) {
            $('#responsive-menu').removeClass('show');
            //alert();
        }
        else{
            $('#responsive-menu').addClass('show'); 
        }
        $('body').tooltip({selector: '[data-toggle="tooltip"]'});
        $(window).on('resize', function () {
            if ($(window).width() < 769) {
                $('#responsive-menu').removeClass('show');
            }
            else{
                $('#responsive-menu').addClass('show'); 
            }
        });

        $(document).on('click','.hamburger-menu', function (e) {
            $("#responsive-searchbar").collapse('hide');
           
        });
        $(document).on('click','.responsive-search', function (e) {
            $("#responsive-menu").collapse('hide');
           
        });
        $(document).on('click','.export-pdf', function (e) {
           var thispdf = $(this); 
            var pagename =  $(this).parent().find('p').text();
            var clone  = $(this).parent().parent().clone();
            clone .find('small').each(function() {
  $(this).replaceWith('<p class="font-size12" style="margin:0px;"><em>'+$(this).text()+'</em></p>');
});
             clone .find('.caption-right-image').each(function() {
  $(this).replaceWith('<p style="margin:0px;"><em>'+$(this).text()+'</em></p>');
});
            clone .find('.export-pdf').remove();
            clone.prepend("<style>.col-header p { font-size: 18px; font-weight: 500; padding: .5rem!important; margin-bottom:0px;}.heading4 { padding: 4px 8px !important; background-color: #bed1e7; color: #000; font-size: 14px;margin-top: 5px; } .col-header { background-color: #c7c7c7; color: #000; display: flex; justifycontent: space-between; align-items: center; } figcaption,.font-size12{ font-style: italic; font-size:13px}</style>");
            var savehtml = clone .html();
            
            var data = {
                action: 'single_page_pdf_generate',
                id: savehtml,
                pagename: pagename
            };  
            $.ajax({
                type: "post",
                dataType: "json",
                url: filter_pages.ajax_url,
                data: data,
                success: function (data) {   
                    thispdf.show();   
                    window.open(data, '_blank');     
                },
                error: function (xhr, status, error) {
                  
                }
            });
           
        });
       $(document).on('click','#material li,#structure-element li,#types-of-investigation li,.desktop-view .breadcrumb li:not(:first-child):not(:nth-child(2)):not(:last-child) a', function (e) {
            //e.preventDefault();
            var selectedelement;
            var parenttarget = $(e.target);
            var target = $(e.target).parent();
            $('.related-technology label').find('img').show(); 
            //$('.related-technology').hide();
            //console.log($(e.target));
             if(target.is('li.breadcrumb-item.materialterm')){
                targetlabel = "Material";
                selectedelement = $('#material');
                if($('.filter-form-mobile').is(":hidden")){
                    $('#material-option').val($(this).attr('id')).trigger("change");
                }
                selectedelement.next().next().next().next().addClass('mb-5p');               
                selectedelement.next().next().next().find('img').show()              
                selectedelement.next().next().next().find('img').attr('title','Select Structural Element to list available options for Target of Investigation.').tooltip('_fixTitle')
               
             }
             else if(target.is('li.breadcrumb-item.structureterm')){
                targetlabel = "structure";
                selectedelement = $('#structure-element');
                if($('.filter-form-mobile').is(":hidden")){
                $('#structure-element-option').val($(this).attr('id')).trigger("change");
                selectedelement.next().next().next().next().removeClass('mb-5p');
                
                }
                selectedelement.next().find('img').hide()
             }
             else if(target.is('li.breadcrumb-item.investigationterm')){
                targetlabel = "structure";
                selectedelement = $('#types-of-investigation');

                if($('.filter-form-mobile').is(":hidden")){
                $('#types-of-investigation-option').val($(this).attr('id')).trigger("change");
                }
             }
             else{
              var targetlabel = $(e.target).closest('ul').prev().text();
              selectedelement = $(this).parent();                            
             }
             
           
            $(this).parent().find('.active').removeClass('active');
            $(this).addClass('active');
            
            
             if(target.is('ul#material.select-filter-list')){
                if($('.filter-form-mobile').is(":hidden")){
                $('#material-option').val($(this).attr('id')).trigger("change");
                }
                selectedelement.next().next().next().next().addClass('mb-5p');  
                selectedelement.next().find('img').hide()              
                selectedelement.next().next().next().find('img').attr('title','Select Structural Element to list available options for Target of Investigation.').tooltip('_fixTitle')
                selectedelement.next().next().next().find('img').show()
                $(this).find('input[type="radio"]').prop("checked", true);
             }
             else if(target.is('ul#structure-element.select-filter-list')){
                if($('.filter-form-mobile').is(":hidden")){
                $('#structure-element-option').val($(this).attr('id')).trigger("change");
                selectedelement.next().next().next().next().removeClass('mb-5p');
               
                }
                selectedelement.next().find('img').hide()
                $(this).find('input[type="radio"]').prop("checked", true);
             }
            else if(target.is('ul#types-of-investigation.select-filter-list')){
                if($('.filter-form-mobile').is(":hidden")){
                $('#types-of-investigation-option').val($(this).attr('id')).trigger("change");
                }
                $(this).find('input[type="radio"]').prop("checked", true);
             }                
            else{
                //alert();
                $(this).prop("checked", true);
            }
            $('.related-technology').find('ul').empty();
            var ids = $(this).attr('id');
            var data = {
                action: 'get_selected_categories',
                id: ids,
                cat_name: targetlabel
            };         

            $.ajax({
                type: "post",
                dataType: "json",
                url: filter_pages.ajax_url,
                data: data,
                success: function (data) {
                    $('.tooltip').hide();                    
                    $('.filtered-content').empty();           
                    console.log(data);
                    //selectedelement.next().hide();
                    //selectedelement.next().next().next().hide();                    
                    if (!target.is('ul#types-of-investigation.select-filter-list')) {                    
                        
                        selectedelement.next().next().removeClass('mb-5p');
                       
                        selectedelement.next().next().empty();
                                           
                        selectedelement.next().next().next().next().empty();

                        $.each(data['cat_result'], function (index, datakey) {
                            selectedelement.next().next().append(datakey);
                        });
                       
                        

                     }  
                     
                     if(target.is('li.breadcrumb-item.investigationterm')|| target.is('ul#types-of-investigation.select-filter-list')){
                        $('.related-technology label').find('img').hide(); 
                        $('.related-technology .card-list').empty();
                        $.each(data['desktop_recommended_tile'], function (index, datapage) {

                            $('.card-list').append(datapage);
                        });    
                        $('#recommended_technologies_mobile').empty();
                        $('#recommended_technologies_mobile').removeClass('disableFilter')
                        $.each(data['mobile_recommended_list'], function (index, datakey) {
                            $('#recommended_technologies_mobile').append(datakey);
                        }); 
                     }
   				    //$('.related-technology').hide();
                    $.each(data['all_pages'], function (index, datapage) {
                        $('.filtered-content').append(datapage);
                    });
                   
                },
                error: function (xhr, status, error) {
                    // var err = eval("(" + xhr.responseText + ")");
                    console.log(xhr.responseText);
                }
            });
        });
        $(document).on('change','#material-option,#structure-element-option,#types-of-investigation-option', function (e) {
            //$('.related-technology').hide();
            //$('.related-technology label').find('img').show(); 
            $('.related-technology').find('ul').empty();
            $('.related-technology').find('select').addClass('disableFilter');
            $('.related-technology').find('select').empty();
            $('.related-technology').find('select').append('<option>All</option>');
            e.preventDefault();
            var target = $(e.target); 
            //console.log(target);
            var ids = $(this).val();
            var dropdowntext = $(this).find('option:selected').text();
            // alert(dropdowntext);
             var selectedelement = $(this);   
            var label =  selectedelement.prev().prev().prev().text();
            if(dropdowntext=="All" && label=="Material"){                
                var ids = selectedelement.prev().prev().val();  
                //alert(ids); 
            }
            if(dropdowntext=="All" && label=="Structural Element"){                
                var ids = selectedelement.prev().prev().val();  
                //alert(ids); 
            }
           

                var data = {
                    action: 'get_selected_categories',
                    id: ids
                };  
                $.ajax({
                    type: "post",
                    dataType: "json",
                    url: filter_pages.ajax_url,
                    data: data,
                    success: function (data) { 
                        console.log(data);                     
                        $('.filtered-content').empty();                        
                        if (target.is('select#material-option.select-filter-list') && dropdowntext=="All") { 
                            $('.reset-form').trigger("click");                          
                        }

                        if (target.is('select#structure-element-option.select-filter-list') && dropdowntext=="All") { 
                            selectedelement.next().next().prop('selectedIndex', 0);
                            selectedelement.next().next().addClass('disableFilter');
                            if($('.filter-form').is(":hidden")){
                                $('#material li[id="'+ids+'"]').trigger("click");
                                }
                        }
                        if (target.is('select#types-of-investigation-option') && dropdowntext=="All") { 
                           
                            if($('.filter-form').is(":hidden")){
                                $('#structure-element li[id="'+ids+'"]').trigger("click");
                                }
                        }

                        if (target.is('select#material-option.select-filter-list') && dropdowntext!="All") {        
                            selectedelement.next().next().removeClass('disableFilter');
                        selectedelement.next().next().next().next().addClass('disableFilter');
                            selectedelement.next().next().empty()    ;                        
                            selectedelement.next().next().next().next().empty();  
                            $.each(data['terms_options'], function (index, datakey) {
                                selectedelement.next().next().append(datakey);
                            });
                            $.each(data['targetoftermsMobile'], function (index, datakey) {
                                selectedelement.next().next().next().next().append(datakey);
                            });
                            if($('.filter-form').is(":hidden")){
                            $('#material li[id="'+ids+'"]').trigger("click");
                            }
                        }  
                        if(target.is('select#structure-element-option.select-filter-list') && dropdowntext!="All"){
                            //alert();
                            selectedelement.next().next().removeClass('disableFilter');
                            selectedelement.next().next().empty();                           
                            selectedelement.next().next().next().next().empty();  
                            $.each(data['terms_options'], function (index, datakey) {
                                selectedelement.next().next().append(datakey);
                            });
                            if($('.filter-form').is(":hidden")){
                            $('#structure-element li[id="'+ids+'"]').trigger("click");
                            }
                        }
                        if(target.is('select#types-of-investigation-option.select-filter-list')&& dropdowntext!="All"){
                            if($('.filter-form').is(":hidden")){
                                $('#types-of-investigation li[id="'+ids+'"]').trigger("click");
                                }
                                $('.related-technology .card-list').empty();
                                $.each(data['desktop_recommended_tile'], function (index, datapage) {
        
                                    $('.card-list').append(datapage);
                                });    
                                $('#recommended_technologies_mobile').empty();
                                $('#recommended_technologies_mobile').removeClass('disableFilter')
                                $.each(data['mobile_recommended_list'], function (index, datakey) {
                                    $('#recommended_technologies_mobile').append(datakey);
                                }); 
                        }
                            
                            $.each(data['all_pages'], function (index, datapage) {
                                $('.filtered-content').append(datapage);
                            });       
                       
                    },
                    error: function (xhr, status, error) {
                        // var err = eval("(" + xhr.responseText + ")");
                        console.log(xhr.responseText);
                    }
                });
            });
       
            $(document).on('click','.mobile-view .breadcrumb li:not(:first-child):not(:nth-child(2)):not(:last-child) a', function (e) {
              //  alert();
              // $('.related-technology').hide();
               $('.related-technology').find('ul').empty();
               $('.related-technology').find('select').addClass('disableFilter');
               $('.related-technology').find('select').empty();
               $('.related-technology').find('select').append('<option>All</option>');
                e.preventDefault();
               
                var target = $(e.target).parent();   
                var targetCheck =  $(e.target);
                //console.log(target)                
               if(target.is('li.breadcrumb-item.materialterm'))  {
                var ids = $(this).attr('id');               
               
                 selectedelement = $('#material-option');              
                 selectedelement.next().next().empty()                                  
                 selectedelement.next().next().next().next().empty()
                 ;
                 if($('.filter-form').is(":hidden")){
                    $('#material li[id="'+ids+'"]').trigger("click");
                    }                 
               }
               else if(target.is('li.breadcrumb-item.structureterm')){
                var ids = $(this).attr('id');
                selectedelement = $('#structure-element-option');               
                selectedelement.next().next().empty();   
                if($('.filter-form').is(":hidden")){
                    $('#structure-element li[id="'+ids+'"]').trigger("click");
                    }
              }
               else if(target.is('li.breadcrumb-item.investigationterm')){
                var ids = $(this).attr('id');
                selectedelement = $('#types-of-investigation-option');
                if($('.filter-form').is(":hidden")){
                    $('#types-of-investigation li[id="'+ids+'"]').trigger("click");
                    }
               }
                       
                    var data = {
                        action: 'get_selected_categories',
                        id: ids
                    };  
                    $.ajax({
                        type: "post",
                        dataType: "json",
                        url: filter_pages.ajax_url,
                        data: data,
                        success: function (data) {                      
                         $('.tooltip').hide();        
                        selectedelement.next().next().empty();
                        selectedelement.next().next().removeClass('disableFilter');
                        selectedelement.next().next().next().next().addClass('disableFilter');
                                selectedelement.next().next().next().next().empty();
                                $('.filtered-content').empty();
                               // console.log(data);
                                $.each(data['terms_options'], function (index, datakey) {
                                    selectedelement.next().next().append(datakey);
                                });
                                $.each(data['targetoftermsMobile'], function (index, datakey) {
                                    selectedelement.next().next().next().next().append(datakey);
                                });
                                $.each(data['all_pages'], function (index, datapage) {
                                    $('.filtered-content').append(datapage);
                                });       
                                //console.log(targetCheck);
                                if(target.is('li.breadcrumb-item.investigationterm')){

                                $('.related-technology .card-list').empty();
                                $.each(data['desktop_recommended_tile'], function (index, datapage) {
        
                                    $('.card-list').append(datapage);
                                });    
                                $('#recommended_technologies_mobile').empty();
                                $('#recommended_technologies_mobile').removeClass('disableFilter')
                                $.each(data['mobile_recommended_list'], function (index, datakey) {
                                    $('#recommended_technologies_mobile').append(datakey);
                                }); 
                            }
                           
                        },
                        error: function (xhr, status, error) {
                            // var err = eval("(" + xhr.responseText + ")");
                            console.log(xhr.responseText);
                        }
                    });
                });


        $(document).on('click change', '.get-single-page', function (e) {
            $('.related-technology label').find('img').show(); 
            var term_tab_id =new Array();
            var target = $(e.target);    
            var showtechnology = false;    
            var  parentid =  $('.reset-form').attr('termid')  ;   
            //alert(parentid);
            //console.log(target);       
           
             
            if($(".filter-form-mobile").is(":hidden")){
                
                 
                  if($('#material li').length && $('#material li').hasClass('active')){
                    var termid = $('#material li.active').attr('id');
                    var material = $('#material li.active').attr('id');;                
                 }    
                 if($('#structure-element li').length && $('#structure-element li').hasClass('active')){     
                    var termid = $('#structure-element li.active').attr('id');;         
                    var structure_element = $('#structure-element li.active').attr('id');                         
    
                 }
                 if($('#types-of-investigation li').length && $('#types-of-investigation li').hasClass('active')){           
              
                    var termid = $('#types-of-investigation li.active').attr('id');
                    if($(".filter-form-mobile").is(":hidden")){
                        showtechnology  =true;   
                        var type_of_investigation = $('#types-of-investigation li.active').attr('id');  
                    }
                   
                }               
                    
            }
            else{ 
                if($("#types-of-investigation-option")[0].selectedIndex  != 0 && $('#types-of-investigation-option option').length != 0 ){
               
                    var termid = $("#types-of-investigation-option").val();
                    if($(".filter-form").is(":hidden")){
                        showtechnology  =true;     
                    }                     
                   
                 }
                 else if($("#structure-element-option").attr("selectedIndex") != 0 && $('#structure-element-option option').length != 0){
                    var termid = $("#structure-element-option").val();
                 }
                 else if($("#material-option").attr("selectedIndex") != 0){
                    var termid = $("#material-option").val();
                 }
                var material = $('#material-option').val();
                var type_of_investigation = $('#types-of-investigation-option').val();
                 var structure_element = $('#structure-element-option').val();
            }
            if(!termid){
                termid =  $('.reset-form ').attr('termid');  
             }
            // Check if the technology dropdown in mobile view  is default selected to All
           
            if(!structure_element || structure_element=="All"){
                structure_element = 0;
            }if(!type_of_investigation || type_of_investigation=="All"){
                type_of_investigation = 0;
            }
            if(!material || material=="All"){
                material = 0;
            }
            // check if the target is from mobile dropdown options
            if(target.is('select#recommended_technologies_mobile.select-filter-list.get-single-page')){
                var post_id = $(this).val();               
            }          
            else{
                var post_id =   $(this).attr('data-postid');  
            }    
            if(!post_id){
                return false;
            }      
             const data = {
                'action': 'nde_get_single_page',
                'parent_id' : parentid,
                'post_id': post_id,
                'term_id':termid,
                'material_id': material,
                'type_investigation':type_of_investigation,
                'structure_element': structure_element
            };
           
            $.ajax({
                url: filter_pages.ajax_url,
                type: 'POST',
                data: data,
                dataType: 'JSON',
                success: function (data) {
                      $('.tooltip').hide();
                    $.each(data['page_result'], function (index, datakey) {
                        $('#single-page-content').html(datakey)
                       

                    });
                    $("#single-page-content .tab-content figure img").each( function (index, datapage) {
                        let title = $(this).attr('title');
                        if(title){
                            $(this).removeAttr('title');
                            $(this).wrap('<span class="tooltip-color" data-toggle="tooltip" data-placement="right" title="'+title+'"></span>');
                        }
                        
                    });
                    if(showtechnology){   
                        $('.related-technology label').find('img').hide();                     
                        $('.related-technology .card-list').empty();
                        $.each(data['all_pages'], function (index, datapage) {

                            $('.card-list').append(datapage);
                        });    
                        $('#recommended_technologies_mobile').empty();
                        $('#recommended_technologies_mobile').removeClass('disableFilter')
                        $.each(data['recommended_technologies_mobile'], function (index, datakey) {
                            $('#recommended_technologies_mobile').append(datakey);
                        }); 
                    }
                  

                    if(!target.is('select#recommended_technologies_mobile.select-filter-list.get-single-page')) {
                        const content = $('#single-page-content').offset().top;
                            $("html, body").animate({scrollTop:content}, "fast");
                        }
                    $('.tab-content a').each(function() {
                        var a = new RegExp('/' + window.location.host + '/');
                        if (!a.test(this.href)) {
                            $(this).attr("target","_blank");
                        }
                    });
                },
                error: function (error) {
                    console.log(error)
                }
            })
        })

     
        $(document).on('click', '.reset-form,.top-term-link a', function () {
            location.reload();
        });

    });

    // Dropdown button for menu item with child
    const mobile_dropdown_btn = '<svg class=\'mobile-menu-plus-icon\' data-name=\'Group 157\' xmlns=\'http://www.w3.org/2000/svg\' width=\'14\' height=\'14\' viewBox=\'0 0 14 14\'>\n' +
        '                                <rect id=\'rectangle_active\' class=\'\' data-name=\'Rectangle 57\' width=\'3\' height=\'14\' transform=\'translate(5.5)\' fill=\'#000\'></rect>\n' +
        '                                <rect id=\'rectangle_hidden\' data-name=\'Rectangle 58\' width=\'3\' height=\'14\' transform=\'translate(14 5.5) rotate(90)\' fill=\'#000\'></rect>\n' +
        '                            </svg>';
    const menu_item_with_child = $('.menu-item-has-children')
    menu_item_with_child.each(function (index, elem){
        //console.log(elem)
        $(mobile_dropdown_btn).appendTo(elem)
    })
    $(menu_item_with_child).off('click')

    menu_item_with_child.find('.mobile-menu-plus-icon').each(function (index, elem){
        $(elem).on('click', function (){
            console.log($(this).prev())
            $(this).prev().toggleClass('display')
            $(this).find('#rectangle_active').toggle('.hide')
        })
    })
    
    $('#primary-menu .menu-item-has-children a[href="#"]').each(function (index, elem){
        $(elem).on('click', function (){
            $(this).next().toggleClass('display')
            console.log($(this).next().next().find('#rectangle_active').toggle('.hide'))
        })
    })

    // Filter button
    $(window).on('click' ,function (){
        const active_class = $('#material li').hasClass('active')
        if (active_class === true){
            $('.reset-form').addClass('display')
        }
    })
     $(document).on('click','.help-collapse-menu svg' ,function (){
       // $(this).prev().toggleClass('display')
            $(this).find('#rectangle_active').toggle('.hide');
            $('.left_sidebar').toggle();

    })
    $('#material-option').on('change', function (){
        $('#material-option option:selected').each(function (index){
            console.log($(this).index())
            if ($(this).index() > 0){
                $('.reset-form').addClass('display')
            }else if ($(this).index() === 0) {
                $('.reset-form').removeClass('display')
            }
        })
    })
    $('.reset-form').on('click', function (){
        $(this).removeClass('display')
    })
    //
  	$("#asset-options").change(function () {
        location.href = jQuery(this).val();
    })
    $('input[type="radio"][name="technology-assettype"]').on('change', function (){
     location.href = jQuery(this).val();
    });

})(jQuery)