<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package nde
 */

get_header();
?>

	<main id="primary" class="site-main search-page">
	<div class="nde-container">
	<div class="col-header">
                <p class="m-0 p-2">Search Results</p>
            </div>
			
			<div class="searchForm">

						<form class="d-flex align-item-center" action="<?php echo home_url() ?>" method="get">
						<?php
						if (isset($_GET['cat-filter']))
						{
							$catfilter = $_GET['cat-filter'];
						}
						else{
							$catfilter = "all";
						}
						?>
							<label class="mb-0 mx-2">Search</label>
							<input type="text" class="border-grey px-1 py-2 font-12 w-100 mr-1" name="s" id="search" placeholder="Search"
								value="<?php the_search_query(); ?>" required/>
								<select name="cat-filter" class="border-grey px-1 py-2 font-12">
								<option value="all" <?php if ($catfilter=="all" ){ echo "selected";} ?>>
								All
								</option>
								<option value="28"  <?php if ($catfilter=="28" ){ echo "selected";} ?>>
								Bridge
								</option>
								<option value="53" <?php if ($catfilter=="53" ){ echo "selected";} ?>>
								Pavements
								</option>
								<option  value="122"  <?php if ($catfilter=="122" ){ echo "selected";} ?>>
								Tunnel
								</option>
								</select>
							<input type="submit" class="img-btn" alt="Search"
								value="Search"/>
						</form>
			</div>
			
		<?php if ( have_posts() ) : ?>

			<header class="page-header mt-3">
				<p>
					<?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Search Results for: %s', 'nde' ), '<span class="fw-6">' . get_search_query() . '</span>' );
					?>
				</p>
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			endwhile;

			the_posts_pagination( array(
				'prev_text' => __( 'Previous ', 'twentyfifteen' ),
				'next_text' => __( 'Next', 'twentyfifteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '', 'twentyfifteen' ) . ' </span>',
				) );
			

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
</div>
	</main><!-- #main -->

<?php
//get_sidebar();
get_footer();