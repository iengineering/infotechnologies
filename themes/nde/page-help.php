<?php
/* Template Name: Help */
get_header();
$page_id = get_queried_object_id();
$post_detail = get_post($page_id);
//var_dump($post_detail);
?>

<div class="nde-container">
    <div class="row m-0 flex-wrap-inherit">
        <div class="col-md-3 px-0 filter-side-bar help-side-menu">
            <div class="col-header filter-form-header help-collapse-menu d-flex align-items-center justify-content-between p-2">
                <p class="m-0 p-0">Help</p>                
                <svg class="mobile-menu-plus-icon" data-name="Group 157" xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                                <rect id="rectangle_active" class="" data-name="Rectangle 57" width="3" height="14" transform="translate(5.5)" fill="#fff" style=""></rect>
                                <rect id="rectangle_hidden" data-name="Rectangle 58" width="3" height="14" transform="translate(14 5.5) rotate(90)" fill="#fff"></rect>
                            </svg>
            </div>
            <div class="left_sidebar">
    
            <a class="<?php if($page_id=="757") echo 'active' ?>" href="<?php echo home_url().'/about' ;?>">
                About
            </a>
            <a class="<?php if($page_id=="759") echo 'active' ?>" href="<?php echo home_url().'/section-508-compliance' ;?>">
               Section 508 Compliance
            </a>
            <a class="<?php if($page_id=="761") echo 'active' ?>" href="<?php echo home_url().'/terms-and-conditions' ;?>">
               Terms and Conditions
            </a>
            <a  target="_blank" class="disclaimer" href="https://www.fhwa.dot.gov/disclaim.cfm">
            Disclaimer and Limitation of Liability
            </a>
            <a class="<?php if($page_id=="250") echo 'active' ?>" href="<?php echo home_url().'/contact-us' ;?>">
               Contact Us
            </a>
            <a class="<?php if($page_id=="770") echo 'active' ?>" href="<?php echo home_url().'/Customer-Support' ;?>">
              Customer Support
            </a>
</div>
          
        </div>

        <div class="col-md-9 pr-0 pl-2 content-col">
            <div class="col-header">
                <p class="m-0 p-2"><?php echo $post_detail->post_title; ?></p>
            </div>
           <div class="page_content <?php if($page_id!="770") echo 'px-2 pt-1' ?> ">
            <?php echo apply_filters( 'the_content', get_post_field('post_content', $page_id) ); ?>
           </div>
        </div>
    </div>
</div>

<?php get_footer() ?>